# Btc-wallet



## Project Detail

This project is about BTC wallet which people can send Bitcoin to your wallet from different countries and different time zones. Moreover, you can query history of your wallet balance at the end of each hour between the DateTime range. The project uses spring boot together with graphql-kotlin-spring-server.

## Getting started
1. Start with `develop branch`
2. Run `Application.kt` directly from your IDE. Alternatively you can also use the Gradle application plugin by running `./gradlew bootRunDev` from the command line.
3. Once the project has started you can explore the example schema by opening the GraphQL Playground endpoint at http://localhost:8082/playground or http://localhost:8082/graphql.

## Example query
```
query{
  showHistoryBtc(startDateTime: "2019-10-03T06:00:01+06:00", endDateTime: "2019-10-05T11:48:01+05:00"){
    records{
      amount
      dateTime
    }
  }
}
```

## Example mutation
```
mutation{
  saveRecord(amount: 1.1, dateTime: "2019-10-05T14:45:05+07:00"){
    amount
    dateTime
  }
}
```


